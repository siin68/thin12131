from odoo import models, fields, api
from datetime import date
from odoo.exceptions import ValidationError
from datetime import datetime


class Booking(models.Model):
    _name = "hotel.booking"
    _description = "Booking"
    _rec_name = "customer_id"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    booking = fields.Char(string="Id customer", help="Lab Result ID", readonly="1", copy=False, index=True)
    customer_id = fields.Many2one('hotel.customer', required=True, string="Customer")
    staff_id = fields.Many2one('hotel.staff', required=True, string="staff")
    room_ids = fields.Many2many('hotel.room', 'booking_id', required=True, string="room")
    currency_id = fields.Many2one('res.currency',
                                  default=lambda self: self.env['res.currency'].search([('name', '=', 'USD')]).id,
                                  readonly=True)
    price_room = fields.Monetary(compute="_compute_total_booking",
                                 string="Price room day")
    price_room_hour = fields.Monetary(compute="_compute_total_booking_hour",
                                      string="Price room hour")

    date_hour_in = fields.Datetime(required=True, string='Date hour in')
    date_hour_out = fields.Datetime(required=True, string='Date hour out')
    number_hour = fields.Integer(string="Number hour booking", compute="_compute_booking_hour")
    deposit = fields.Monetary(string="Deposit")
    discount = fields.Float(related="customer_id.discount", string="discount")
    total_price_hour = fields.Monetary(compute="_compute_total_hour",
                                       string="total price hour")
    # evaluate_ids = fields.One2many('hotel.report', 'customer_id',
    #                                string="evaluate")

    state_book = fields.Selection([
        ('open', ' Open'),
        ('booked', 'Booked'),
        ('leave', ' Leave'),
        ('cancel', 'Cancel')],
        default='open',
        string="Status")

    @api.depends('number_hour', 'price_room_hour')
    def _compute_total_hour(self):
        for rec in self:
            if rec.number_hour and rec.price_room_hour:
                rec.total_price_hour = rec.price_room_hour * rec.number_hour
            else:
                rec.total_price_hour = 0

    @api.model
    def create(self, vals):

        vals['booking'] = self.env['ir.sequence'].next_by_code('hotel.booking.sequence')
        res = super(Booking, self).create(vals)
        # res.room_ids.room_status = 'booked'
        # res.state_book = 'booked'
        return res

    @api.depends("room_ids.price_room_hour")
    def _compute_total_booking_hour(self):
        for rec in self:
            rec.price_room_hour = sum(rec.price_room_hour for rec in rec.room_ids)

    @api.depends('date_hour_in', 'date_hour_out')
    def _compute_booking_hour(self):
        for rec in self:
            if rec.date_hour_out and rec.date_hour_in:
                rec.number_hour = (((rec.date_hour_out.day - rec.date_hour_in.day) * 24) + (
                        rec.date_hour_out.hour - rec.date_hour_in.hour))
            else:
                rec.number_hour = 0

    def action_booked(self):
        for rec in self:
            rec.state_book = 'booked'
            rec.room_ids.room_status = 'booked'

    def action_leave(self):
        for rec in self:
            rec.state_book = 'leave'
            rec.room_ids.room_status = 'empty'

    def action_cancel(self):
        for rec in self:
            rec.state_book = 'cancel'

    def action_open(self):
        for rec in self:
            rec.state_book = 'open'
            rec.room_ids.room_status = 'empty'

    # @api.onchange('date_hour_out', 'date_out')
    # def onchange_out(self):
    #     for rec in self:
    #         today = date.today()
    #         if rec.date_out and rec.date_out.day == today.day:
    #             rec.state_book = "leave"
    #             rec.room_ids.room_status = "empty"
    #         if rec.date_hour_out.day and rec.date_hour_out.day == today.day and rec.date_hour_out.hour == today.hour:
    #             rec.state_book = "leave"
    #             rec.room_ids.room_status = "empty"

    @api.constrains('date_hour_out')
    def _check_date_of_birth(self):
        for rec in self:
            today = datetime.today()
            if rec.date_hour_out and rec.date_hour_out.day <= today.day and rec.date_hour_out.hour <= today.hour:
                raise ValidationError("The date and time must be greater than or equal to the current time")

    @api.constrains('date_hour_in')
    def _check_date_of_birth(self):
        for rec in self:
            today = datetime.today()
            if rec.date_hour_in and rec.date_hour_out.day < today.day and rec.date_hour_out.hour <= today.hour:
                raise ValidationError("The date and time must be greater than the present")
