from odoo import models, fields, api


class Service(models.Model):
    _name = "hotel.service"
    _description = "Service"
    _rec_name = "name_service"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    service_id = fields.Char(string="ID room type", readonly=True, coppy=False, default="New", required=True)
    name_service = fields.Char(string="Name service", required=True)
    currency_id = fields.Many2one('res.currency',
                                  default=lambda self: self.env['res.currency'].search([('name', '=', 'USD')]).id,
                                  readonly=True)
    price = fields.Monetary(string="Price service")
    color = fields.Integer(string="Color")


    @api.model
    def create(self, vals):
        vals['service_id'] = self.env['ir.sequence'].next_by_code('hotel.service.sequence')
        return super(Service, self).create(vals)
