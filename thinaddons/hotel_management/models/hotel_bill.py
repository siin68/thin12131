from odoo import models, fields, api
from datetime import date
from odoo.exceptions import ValidationError


class Bill(models.Model):
    _name = "hotel.bill"
    _description = "Bill"
    _rec_name = "booking_id"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    bill = fields.Char(string="Id Bill", help="Lab Result ID", readonly="1", copy=False, index=True)
    booking_id = fields.Many2one('hotel.booking', string="booking", required=True)
    currency_id = fields.Many2one('res.currency',
                                  default=lambda self: self.env['res.currency'].search([('name', '=', 'USD')]).id,
                                  readonly=True)
    price_room_hour = fields.Monetary(related="booking_id.total_price_hour", string="Price room hour")

    discount = fields.Float(relate="booking_id.discount", string="discount")
    deposit = fields.Monetary(relate="booking_id.deposit", string="Deposit")
    staff_id = fields.Many2one('hotel.staff', string="hihi")
    total_amount = fields.Monetary(compute="_compute_total", string="Total Bill")
    bill_date = fields.Date(string="Bill date", default=date.today())
    total_sv_ids = fields.One2many('hotel.total.service', 'bill_id', string="order service")
    total_pharmacy = fields.Monetary(string='Total', compute="_compute_total_service")
    state_bill = fields.Selection([
        ('draft', ' Draft'),
        ('unpaid', 'Unpaid'),
        ('paid', ' Paid'),
        ('cancel', 'Cancel')],
        default='draft',
        string="Status")

    def action_draft(self):
        for rec in self:
            rec.state_bill = 'draft'

    def action_unpaid(self):
        for rec in self:
            rec.state_bill = 'unpaid'

    def action_cancel(self):
        for rec in self:
            rec.state_bill = 'cancel'

    def unlink(self):
        for rec in self:
            if rec.state_bill == 'paid':
                raise ValidationError('You cannot delete paid bills!')
        return super(Bill, self).unlink()

    @api.depends('total_sv_ids')
    def _compute_total_service(self):
        for rec in self:
            rec.total_pharmacy = sum(rec.total_sv_ids.mapped("total"))

    @api.model
    def create(self, vals):
        vals['bill'] = self.env['ir.sequence'].next_by_code('hotel.bill.sequence')
        return super(Bill, self).create(vals)

    @api.onchange('booking_id')
    def onchange_deposit(self):
        self.deposit = self.booking_id.deposit

    @api.onchange('booking_id')
    def onchange_discount(self):
        self.discount = self.booking_id.discount

    @api.depends('discount', 'deposit', 'price_room_hour', 'total_pharmacy')
    def _compute_total(self):
        for rec in self:
            rec.total_amount = 0
            if rec.price_room_hour:
                rec.total_amount = ((rec.price_room_hour - (
                        rec.price_room_hour * rec.discount / 100)) - rec.deposit) + rec.total_pharmacy


class TotalSV(models.Model):
    _name = "hotel.total.service"
    _description = "Order service"
    service_id = fields.Many2one('hotel.service', reqired=True, string="Name service")
    currency_id = fields.Many2one('res.currency',
                                  default=lambda self: self.env['res.currency'].search([('name', '=', 'USD')]).id,
                                  readonly=True)
    price_service = fields.Monetary(related='service_id.price', string="Price service")
    qty = fields.Integer(string='Quantity', default=1)
    total = fields.Monetary(string='Subtotal', compute='compute_subtotal')
    bill_id = fields.Many2one('hotel.bill', string='Bill')

    @api.depends('price_service', 'qty')
    def compute_subtotal(self):
        for rec in self:
            rec.total = (rec.price_service * rec.qty)
