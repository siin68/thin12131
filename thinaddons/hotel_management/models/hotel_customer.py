from importlib.resources import _

from odoo import models, fields, api
from datetime import date
from odoo.exceptions import ValidationError


class Customer(models.Model):
    _name = "hotel.customer"
    _description = "Customer"
    _rec_name = "name_customer_id"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    customer = fields.Char(string="Id customer", help="Lab Result ID", readonly="1", copy=False, index=True, store=True)
    date_of_birth = fields.Date(string='Date of Birth')
    age = fields.Integer(string="Age", compute='_compute_age', tracking=True)
    gender = fields.Selection([('male', 'Male'), ('female', 'Female'), ('other', 'Other')], string='Gender',
                              default='male')
    name_customer_id = fields.Many2one('res.users', required=True, string="Name customer",
                                       default=lambda self: self.env.user.id)
    address = fields.Char(string="Address")
    phone_number = fields.Char(related='name_customer_id.mobile', string="Phone number")
    email = fields.Char(related='name_customer_id.login', string="Account email")
    status = fields.Char(string="Note")
    point_ids = fields.Many2one('hotel.point', string="Point")
    discount = fields.Float(related="point_ids.discount", string="Discount")

    @api.model
    def create(self, vals):

        vals['customer'] = self.env['ir.sequence'].next_by_code('hotel.customer.sequence')
        return super(Customer, self).create(vals)

    @api.depends('date_of_birth')
    def _compute_age(self):
        for rec in self:
            today = date.today()
            print("date.today()", date.today())
            if rec.date_of_birth:
                rec.age = today.year - rec.date_of_birth.year
            else:
                rec.age = 0



    @api.constrains('date_of_birth')
    def _check_date_of_birth(self):
        for rec in self:
            if rec.age and rec.age <= 18:
                raise ValidationError("Please enter your correct age and more than 18!")

    def action_view_booking(self):
        return {
            'name': _('Point'),
            'type': 'ir.actions.act_window',
            'res_model': 'hotel.point',
            'view_mode': 'tree,form',
            'target': 'current',
            'context': {
            },
        }

