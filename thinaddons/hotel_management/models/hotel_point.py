from odoo import models, fields, api


class Point(models.Model):
    _name = "hotel.point"
    _description = "point"
    _rec_name = "point"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    point_cus = fields.Char(string="Id Point", readonly=True, coppy=False, default="New", required=True)
    point = fields.Selection(
        [('0', '0'),
         ('1 ', '1'),
         ('2', '2'),
         ('3 ', '3'),
         ('4 ', '4'),
         ('5 ', '5'),
         ],
        string="Point",
        default='Normal room')
    discount = fields.Float(string="Discount")
    customer_ids = fields.One2many('hotel.customer', 'point_ids', string="customer")

    @api.model
    def create(self, values):
        if values.get('point_cus', 'New') == 'New':
            values['point_cus'] = self.env['ir.sequence'].next_by_code('hotel.point.sequence') or 'New'
            result = super(Point, self).create(values)
            return result
