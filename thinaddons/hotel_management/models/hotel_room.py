from odoo import models, fields, api
from odoo.exceptions import ValidationError


class Room(models.Model):
    _name = "hotel.room"
    _description = "Room"
    _rec_name = "name_room"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    room = fields.Char(string="ID room", readonly=True, coppy=False, default="New", required=True)
    name_room = fields.Char(string=" Name room", required=True)
    image_room = fields.Binary(string="Image")
    booking_id = fields.Many2one('hotel.booking', string="booking")
    room_type_id = fields.Many2one("hotel.room_type", string="Room type")
    currency_id = fields.Many2one('res.currency',
                                  default=lambda self: self.env['res.currency'].search([('name', '=', 'USD')]).id,
                                  readonly=True)

    price_room_hour = fields.Monetary(related="room_type_id.price_room_hour", string="Price room hour")

    status = fields.Char(string="Note")
    color = fields.Integer(string="Color")
    room_status = fields.Selection([('empty', 'empty'), ('booked', 'booked')], default='empty', string="Status room")

    @api.model
    def create(self, vals):
        vals['room'] = self.env['ir.sequence'].next_by_code('hotel.room.sequence')
        return super(Room, self).create(vals)

    @api.constrains('name_room')
    def _check_name(self):
        for rec in self:
            products = self.env['hotel.room'].search([('name_room', '=', rec.name_room), ('id', '!=', rec.id)])
            if products:
                raise ValidationError('Name %s Already Exists' % rec.name_room)
