from odoo import models, fields, api
from datetime import date
from odoo.exceptions import ValidationError


class Staff(models.Model):
    _name = "hotel.staff"
    _inherit = "hotel.customer"
    _description = "Customer"

    image = fields.Binary(related='name_customer_id.image_1920', string="Image")
    state = fields.Selection([
        ('Working', ' Working'),
        ('Leave', 'Leave'), ],
        default='Working',
        string="State", index=True)
    bill_ids = fields.Many2many('hotel.bill', 'staff_id', string="Bill", readonly=True, store=True,
                                compute="_compute_bill")
    date_start = fields.Date(string='Date start')
    date_end = fields.Date(string='Date end')

    @api.constrains('date_of_birth')
    def _check_date_of_birth(self):
        for rec in self:
            if rec.age and rec.age <= 18:
                raise ValidationError("Please enter your correct age and more than 18!")

    @api.depends('name_customer_id', 'date_start', 'date_end')
    def _compute_bill(self):
        for rec in self:
            rec.bill_ids = self.env['hotel.bill'].search(
                [('booking_id.staff_id', '=', rec.name_customer_id.name), ('bill_date', '>=', rec.date_start),
                 ('bill_date', '<=', rec.date_end), ('state_bill', '= ', 'paid')])
