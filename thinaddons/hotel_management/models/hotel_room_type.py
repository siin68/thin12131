from odoo import models, fields, api
from odoo.exceptions import ValidationError


class Roomtype(models.Model):
    _name = "hotel.room_type"
    _description = "room type"
    _rec_name = "type_room"

    room_type = fields.Char(string="ID room type", readonly=True, coppy=False, default="New", required=True)
    type_room = fields.Char(string="Room type", required=True)
    currency_id = fields.Many2one('res.currency',
                                  default=lambda self: self.env['res.currency'].search([('name', '=', 'USD')]).id,
                                  readonly=True)

    room_ids = fields.One2many('hotel.room', 'room_type_id', string="room")
    price_room_hour = fields.Monetary(string="Price room hour")

    @api.model
    def create(self, vals):
        vals['room_type'] = self.env['ir.sequence'].next_by_code('hotel.room_type.sequence')
        return super(Roomtype, self).create(vals)

    @api.constrains('type_room')
    def _check_name_type(self):
        for rec in self:
            products = self.env['hotel.room_type'].search([('type_room', '=', rec.type_room), ('id', '!=', rec.id)])
            if products:
                raise ValidationError('Name type %s Already Exists' % rec.type_room)
