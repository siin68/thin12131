from odoo import models, fields, api
from datetime import date, datetime


class Service(models.Model):
    _name = "hotel.report"
    _description = "Report"
    _rec_name = "report"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    report = fields.Char(string="ID room type", readonly=True, coppy=False, default="New", required=True)
    booking_id = fields.Many2one('hotel.booking', string="Name customer")
    staff = fields.Many2one(related='booking_id.staff_id', string="Name staff")
    room = fields.Many2many(related='booking_id.room_ids', string="Rooms")

    content_report = fields.Char(string="Content")
    level = fields.Selection(
        [('0', '0'),
         (' 1', '1'),
         ('2', '2'),
         ('3 ', '3'),
         ('4 ', '4'),
         ('5 ', '5'),
         ],
        string="Point",
    )
    report_date = fields.Date(string="Report date", default=date.today())

    @api.model
    def create(self, vals):
        vals['report'] = self.env['ir.sequence'].next_by_code('hotel.report.sequence')
        return super(Service, self).create(vals)
