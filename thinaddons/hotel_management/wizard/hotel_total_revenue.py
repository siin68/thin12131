from odoo import models, fields, api
from datetime import date


class Revenue(models.TransientModel):
    _name = "hotel.total.revenue"
    _description = "Revenue"

    revenue = fields.Char(string="Id Bill", help="Lab Result ID", readonly="1", copy=False, index=True)
    bill_ids = fields.Many2many('hotel.bill', string="Bill", readonly=True, store=True, compute='_compute_bill')
    date_start = fields.Date(string='Date start')
    date_end = fields.Date(string='Date end')
    currency_id = fields.Many2one('res.currency',
                                  default=lambda self: self.env['res.currency'].search([('name', '=', 'USD')]).id,
                                  readonly=True)
    price_unpaid = fields.Monetary(string="Price Unpaid", compute="_compute_total_paid")
    price_paid = fields.Monetary(string="Price Paid", compute="_compute_total_paid")
    state_revenue = fields.Selection([
        ('draft', ' Draft'),
        ('unpaid', 'Unpaid'),
        ('paid', ' Paid'),
        ('all', 'All'),
    ],
        default='paid',
        string="Status")

    @api.model
    def create(self, vals):
        vals['revenue'] = self.env['ir.sequence'].next_by_code('hotel.revenue.sequence')
        return super().create(vals)

    @api.depends('bill_ids', 'bill_ids.total_amount')
    def _compute_total_paid(self):
        for rec in self:
            rec.price_unpaid = sum(
                rec.bill_ids.filtered(lambda x: x.state_bill in ('draft', 'unpaid')).mapped("total_amount"))
            rec.price_paid = sum(rec.bill_ids.filtered(lambda x: x.state_bill == 'paid').mapped("total_amount"))

    # @api.onchange('date_start', 'date_end', 'state_revenue')
    # def test(self):
    #     bill_ids = self.env['hotel.bill'].search(
    #         [('bill_date', '>=', self.date_start), ('bill_date', '<=', self.date_end),
    #          ('state_bill', '=', self.state_revenue)])
    #     if bill_ids:
    #         self.bill_ids = bill_ids.ids
    #     elif self.state_revenue == 'all':
    #         self.bill_ids = self.bill_ids

    @api.depends('state_revenue', 'date_start', 'date_end')
    def _compute_bill(self):
        for rec in self:

            # if not rec.date_end or not rec.date_start:
            #     rec.bill_ids = False
            #     return

            domain = [('bill_date', '>=', self.date_start), ('bill_date', '<=', self.date_end)]
            if rec.state_revenue != 'all':
                domain.append(('state_bill', '=', rec.state_revenue))

            rec.bill_ids = self.env['hotel.bill'].search(domain)


