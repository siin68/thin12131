from odoo import api, fields, models
from datetime import datetime
from odoo.exceptions import ValidationError


class check_bill(models.TransientModel):
    _name = 'hotel.check.bill.wizard'
    _description = 'Create new payment'

    bill_ids = fields.Many2one('hotel.bill', string="Bill ")
    currency_id = fields.Many2one('res.currency',
                                  default=lambda self: self.env['res.currency'].search([('name', '=', 'USD')]).id,
                                  readonly=True)
    price_room_hour = fields.Monetary(related="bill_ids.price_room_hour", string="Price room hour")
    deposit = fields.Monetary(related="bill_ids.deposit", string="Deposit")

    total_amount = fields.Monetary(compute="_compute_total", string="total_amount")

    @api.depends('deposit', 'price_room_hour')
    def _compute_total(self):
        for rec in self:
            rec.total_amount = 0
            if rec.price_room_hour:
                rec.total_amount = ((rec.price_room_hour) - (
                        (rec.price_room_hour) / 100)) - rec.deposit
