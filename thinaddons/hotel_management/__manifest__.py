# -*- coding: utf-8 -*-


{
    'name': 'Hotel',
    'version': '15.0.1.0.0',
    'category': 'Hotel BNK',
    'author': 'By  Thin BNK',
    'summary': 'so verygood',
    'description': """""",
    'depends': ['mail'],
    'demo': [],
    'sequence': -100,
    'assets': {},
    'data': [
        'views/hotel_root_menu_views.xml',
        'views/hotel_point_views.xml',
        'views/hotel_customer_views.xml',
        'views/hotel_room_type_views.xml',
        'views/hotel_room_views.xml',
        'views/hotel_staff_views.xml',
        'views/hotel_service_views.xml',
        'views/hotel_evaluate_views.xml',
        'views/hotel_booking_views.xml',
        'wizard/hotel_bill_check_views.xml',
        'views/hotel_bill_views.xml',
        'security/security.xml',
        'security/ir.model.access.csv',
        'report/report.xml',
        'report/hotel_report_bill.xml',

        'wizard/hotel_total_revenue.xml',
        'wizard/hotel_bill_paid.xml',
    ],
    'license': 'LGPL-3',
    'auto_install': False,
    'installable': True,
    'application': True,
}
